﻿using System;
using GemBox.Document;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace Lab3.Tests
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void DecryptoTest_stringIn_stringOut()
        {
            
            int shift = 2;
            string text = "asdабвАБВ123+))_*&";

            string result = Crypto.Decrypto(text, shift);

            Assert.IsInstanceOfType(result,  typeof(string));
        }

        [TestMethod]
        public void DecryptoTest_cryptoIn_decryptedOut()
        {
            int shift = 168;
            string text = "asdабвАБВ123+))_*&";

            string result = Crypto.Decrypto(text, shift);

            Assert.AreEqual(result, "asdгдеГДЕ123+))_*&");
        }
        

        [TestMethod]
        public void ShiftCheker_lessZero_bigger()
        {
            int shift = -2;
            int result = Crypto.ShiftCheck(shift);

            Assert.AreEqual(result, shift + 33);
        }
        [TestMethod]
        public void ShiftCheker_ThreeThree_zero()
        {
            int shift = 33;
            int result = Crypto.ShiftCheck(shift);

            Assert.AreEqual(result, 0);
        }
        [TestMethod]
        public void ShiftCheker_BiggerZero_less()
        {
            int shift = 34;
            int result = Crypto.ShiftCheck(shift);

            Assert.AreEqual(result, shift - 33);
        }
        [TestMethod]
        public void ShiftCheker_InInterval_Same()
        {
            int shift = 15;
            int result = Crypto.ShiftCheck(shift);

            Assert.AreEqual(result, shift);
        }


    }
}
