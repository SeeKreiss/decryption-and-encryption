﻿using GemBox.Document;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Lab3
{
    public class Crypto
    {
        public static string Decrypto(string text, int shift)
        {
            string result = "";
            string txt = "";
            string abc = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            string ABC = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
            txt = text;
            
            shift = shift % 33;
            for (int i = 0; i < txt.Length; i++)
            {
                int k = 0;
                //Если не кириллица и не число
                if (!(abc.Contains(txt[i].ToString()) || ABC.Contains(txt[i].ToString())))
                    result += txt[i];
                else
                {
                    for (int j = 0; j < abc.Length; j++)
                    {
                        if (abc[j].Equals(txt[i]) || ABC[j].Equals(txt[i]))
                            k = j;
                    }
                    
                    //Если буква является строчной
                    if (abc.Contains(txt[i].ToString()))
                    {
                        //Если буква, после сдвига выходит за пределы алфавита
                        if (k + shift >= abc.Length)
                            //Добавление в строку результатов символ
                            result += abc[k + shift - abc.Length];
                        //Если буква может быть сдвинута в пределах алфавита
                        else
                            //Добавление в строку результатов символ
                            result += abc[k + shift];
                    }
                    //Если буква является прописной
                    if (ABC.Contains(txt[i].ToString()))
                    {
                        //Если буква, после сдвига выходит за пределы алфавита
                        if (k + shift >= ABC.Length)
                            //Добавление в строку результатов символ
                            result += ABC[k + shift - ABC.Length];
                        //Если буква может быть сдвинута в пределах алфавита
                        else
                            //Добавление в строку результатов символ
                            result += ABC[k + shift];
                    }
                    
                }

            }
            return result;
        }
        public static string FromDocx(string path)
        {
            var document = new DocumentModel();
            document = DocumentModel.Load(path);
            StringBuilder sb = new StringBuilder();

            foreach (Paragraph paragraph in document.GetChildElements(true, ElementType.Paragraph))
            {
                foreach (Run run in paragraph.GetChildElements(true, ElementType.Run))
                {
                    bool isBold = run.CharacterFormat.Bold;
                    string text1 = run.Text;

                    sb.AppendFormat("{0}{1}{2}", isBold ? "<b>" : "", text1, isBold ? "</b>" : "");
                }
                sb.AppendLine();
            }
            return sb.ToString();
        }
        public static int ShiftCheck(int shift)
        {
            if (shift >= 33)
                shift -= 33;
            if (shift < 0)
                shift += 33;
            return shift;
        }
    }

}