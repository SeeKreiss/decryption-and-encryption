﻿using GemBox.Document;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Lab3
{
    public partial class Decrypt : System.Web.UI.Page
    {
        static string text;
        static int shift = 0;
        static string filepath;
        protected void Page_Load(object sender, EventArgs e)
        {
            cryptotxt.Visible = true;
            TextBox1.Visible = true;
            TextBox2.Text = "0";
        }


        protected void Button1_Click(object sender, EventArgs e) //кнопка показать
        {
            try
            {
                filepath = Server.MapPath("~/upload") + Path.GetFileName(FileUpload1.FileName);
                FileUpload1.SaveAs(filepath);
                if (Path.GetExtension(filepath).Equals(".txt"))
                {
                    text = File.ReadAllText(filepath);
                    cryptotxt.Text = text;
                    TextBox1.Text = text;
                }
                else if (Path.GetExtension(filepath).Equals(".docx"))
                {

                    text = Crypto.FromDocx(filepath);
                    cryptotxt.Text = text;
                    TextBox1.Text = text;
                }
                else
                {
                    cryptotxt.Text = "Неверный формат файла";
                }
                File.Delete(filepath);
            }
            catch (Exception ex) { }

        }


        protected void Button3_Click(object sender, EventArgs e) //стрелка вперед
        {
            shift++;
            shift = Crypto.ShiftCheck(shift);

            TextBox2.Text = shift.ToString();
            TextBox1.Text = Crypto.Decrypto(text, shift);
        }

        protected void Button2_Click1(object sender, EventArgs e)//стрелка назад
        {
            shift--;
            shift = Crypto.ShiftCheck(shift);

            TextBox2.Text = shift.ToString();
            TextBox1.Text = Crypto.Decrypto(text, shift);
        }

        protected void Button4_Click(object sender, EventArgs e)//сохранить на диск
        {
            try
            {
                if (TextBox1.Text != null)
                {
                    string boxText = TextBox1.Text;
                    
                    if (Path.GetExtension(filepath).Equals(".txt"))
                    {
                        Response.ContentType = "text/plain";
                        Response.AppendHeader("Content-Disposition", "attachment; filename=Decrypted.txt");
                        Response.Write(boxText);
                        Response.End();
                    }
                    else if (Path.GetExtension(filepath).Equals(".docx"))
                    {
                        Response.ContentType = "application/msword";
                        Response.AppendHeader("Content-Disposition", "attachment; filename=Decrypted.Docx");
                        var document = new DocumentModel();
                        document.Sections.Add(new Section(document, new Paragraph(document, boxText)));
                        document.Save(Server.MapPath("~/") + "Encrypted.docx");
                        Response.TransmitFile(Server.MapPath("~/") + "Encrypted.docx");
                        Response.End();
                        File.Delete(Server.MapPath("~/") + "Encrypted.docx");
                    }
                }
            }
            catch
            { }

        }
    }
}