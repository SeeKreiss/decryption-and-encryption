﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Lab3.Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            height: 51px;
            width: 884px;
        }
        .auto-style3 {
            width: 893px;
        }
        </style>
</head>
<body style="width: 888px; margin-left: 300px; height: 331px;">
    <form id="form1" runat="server">
        <div class="auto-style1" style="background-color: #999999; border-style: solid; border-color: #000000">
            <asp:Button ID="Button1" PostBackUrl="~/Decrypt.aspx" runat="server" Height="50px" Text="Дешифровать" Width="260px" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button2" PostBackUrl="~/HandEncrypt.aspx" runat="server" Height="50px" Text="Шифровать тест с клавиатуры" Width="260px" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button3" PostBackUrl="~/FileEncrypt.aspx" runat="server" Height="50px" Text="Шифровать текст из файла" Width="260px" />
        </div>
        <p>
            &nbsp;</p>
             <table style="border-style: double; border-color: #000000">
            <tr><td class="auto-style3" style="font-family: Arial">Здравствуйте, вы находитесь на домашней странице приложения.<br />
                Приложение выполняет функции дешифратора и шифратора.<br />
                Считывает зашифрованный файл и дешифрует его, либо шифрует введенный текст или текст считанный из файла.
                <br />
                <br />
                У этого приложения есть три основных возможности:
                <br />
                1. Дешифровка текста из .txt и .docx файлов.
                <br />
                2. Шифрование текста введенного пользователем с клавиатуры.
                <br />
                3. Шифрование текста считанноо из .txt и .docx файлов.
                <br />
                <br />
                Шифрование и дешифрование происходит методом простой перестановки с постоянным шагом сдвига. Шифром Цезаря. Шифруются только символы кириллицы.
                </td></tr>
                 
        </table>
    </form>
</body>
</html>
