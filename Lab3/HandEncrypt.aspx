﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HandEncrypt.aspx.cs" Inherits="Lab3.HandEncrypt" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style type="text/css">
        .auto-style1 {
            width: 1000px;
        }
        .auto-style2 {
            width: 1006px;
        }
        .auto-style5 {
            width: 1889px;
        }
    </style>

</head>
<body style="margin-left: 300px">
    <form id="form1" runat="server" class="auto-style2" style="border-style: outset">
        <div style="font-family: Arial">
             <table>
            <tr><td class="auto-style5" style="font-family: Arial" rowspan="1">Чтобы зашифровать текст введите текст, выберите шаг и направление сдвига по алфавиту, и нажмите кнопку &quot;Шифровать&quot;.<br />
            Чтобы сохранить полученный шифр в файл на жестком диске нажмите на кнопку &quot;Сохранить шифрованный текст&quot;<br />
            <br />
            Введите текст:</td></tr>
                 
        </table>
        </div>
        <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Font-Names="Arial" Height="200px" Width="1000px" BackColor="White" BorderColor="#999999" BorderStyle="Solid"></asp:TextBox>
        <p style="font-family: Arial">
            Введите шаг сдвига и выберите направление сдвига:</p>
        <p>
            <asp:TextBox ID="TextBox2" runat="server" Font-Names="Arial" BackColor="White" BorderColor="#999999" BorderStyle="Solid"></asp:TextBox>
            <asp:RadioButtonList ID="RadioButtonList1" runat="server" Font-Names="Arial" BorderColor="#CCCCCC" BorderStyle="Double">
                <asp:ListItem>Назад</asp:ListItem>
                <asp:ListItem Selected="True">Вперед</asp:ListItem>
            </asp:RadioButtonList>
        </p>
        <p>
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" style="height: 26px" Text="Шифровать" />
        </p>
        <p>
            <asp:TextBox ID="TextBox3" runat="server" TextMode="MultiLine" Height="200px" ReadOnly="True" Width="1000px" Font-Names="Arial" BackColor="White" BorderColor="#999999" BorderStyle="Solid"></asp:TextBox>
        </p>
        <p class="auto-style1">
            <asp:Button ID="Button4" runat="server" Font-Names="Arial" OnClick="Button4_Click" Text="Сохранить шифрованный текст" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:ImageButton ID="ImageButton1" runat="server" PostBackUrl="~/Home.aspx" BorderStyle="Ridge" Height="50px" ImageUrl="~/home.png" Width="50px" />
        </p>
    </form>
</body>
</html>
