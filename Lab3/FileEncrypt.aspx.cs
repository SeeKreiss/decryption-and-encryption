﻿using GemBox.Document;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Lab3
{
    public partial class FileEncrypt : System.Web.UI.Page
    {
        static string text;
        static int shift = 0;
        static string filepath;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e) //считывание текста
        {
            try
            {
                filepath = Server.MapPath("~/upload") + Path.GetFileName(FileUpload1.FileName);
                FileUpload1.SaveAs(filepath);
                if (Path.GetExtension(filepath).Equals(".txt")) //если текстовый файл
                {
                    text = File.ReadAllText(filepath);
                    TextBox1.Text = text;
                    TextBox3.Text = text;
                }
                else if (Path.GetExtension(filepath).Equals(".docx")) //если docx файл
                {
                    
                    text = Crypto.FromDocx(filepath);
                    TextBox1.Text = text;
                    TextBox3.Text = text;
                }
                else
                {
                    TextBox1.Text = "Неверный формат файла";
                }
                File.Delete(filepath);


            }
            catch (Exception ex) { }
        }

        protected void Button2_Click(object sender, EventArgs e)  //шифрование
        {
            if (Int32.TryParse((TextBox2.Text), out shift))
            {
                if (shift >= 0)
                {
                    if (RadioButtonList1.SelectedIndex == 0)
                    {
                        shift *= -1;
                    }
                    if (shift < 0)
                    {
                        shift += 33;
                    }
                    text = TextBox1.Text;
                    TextBox3.Text = Crypto.Decrypto(text, shift);
                }
                else
                {
                    TextBox3.Text = "Введите положительное число";
                }

            }
            else
                TextBox3.Text = "Введите положительное число";
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            string result = TextBox3.Text;
            try
            {
                if (result != null)
                {
                    if (Path.GetExtension(filepath).Equals(".txt")) //если изначально был текстовый файл
                    {
                        Response.ContentType = "text/plain";
                        Response.AppendHeader("Content-Disposition", "attachment; filename=Decrypted.txt");
                        Response.Write(result);
                        Response.End();
                    }
                    else if (Path.GetExtension(filepath).Equals(".docx")) //если изначально был Docx файл
                    {
                        Response.ContentType = "application/msword";
                        Response.AppendHeader("Content-Disposition", "attachment; filename=Decrypted.Docx");
                        var document = new DocumentModel();
                        document.Sections.Add(new Section(document, new Paragraph(document, result)));
                        document.Save(Server.MapPath("~/") + "Encrypted.docx");
                        Response.TransmitFile(Server.MapPath("~/") + "Encrypted.docx");
                        Response.End();
                        File.Delete(Server.MapPath("~/") + "Encrypted.docx");
                    }
                }
            }
            catch
            { }
        }
    }
}