﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FileEncrypt.aspx.cs" Inherits="Lab3.FileEncrypt" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 1007px;
        }
        .auto-style3 {
            width: 893px;
        }
        .auto-style5 {
            width: 893px;
            height: 27px;
        }
        .auto-style6 {
            width: 1225px;
            height: 27px;
        }
        .auto-style7 {
            width: 1005px;
        }
    </style>
</head>
<body style="margin-left: 300px">
    <form id="form1" runat="server" class="auto-style7" style="border-style: solid; border-color: #999999">
        <div class="auto-style1">
             <table>
            <tr><td class="auto-style3" style="font-family: Arial" colspan="2">Чтобы загрузить файл для расшифровки нажмите кнопку &quot;Choose File&quot;.
                <br />
            Чтобы зашифровать выберите шаг и направление сдвига по алфавиту, и нажмите кнопку &quot;Шифровать&quot;.<br />
            Чтобы сохранить полученный шифр в файл на жестком диске нажмите на кнопку &quot;Сохранить шифрованный текст&quot;.</td></tr>
             <tr><td class="auto-style5" style="font-family: Arial">Загрузить файл :</td><td class="auto-style6"><asp:FileUpload ID="FileUpload1" runat="server" accept=".txt, .docx" Font-Names="Arial" BorderColor="#999999" BorderStyle="Solid" />
                 <br />
                 <asp:Button ID="Button5" runat="server" Text="Показать" Height="24px" OnClick="Button1_Click" Width="127px" Font-Names="Arial" /></td></tr>
                 
        </table>
        </div>
        <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Font-Names="Arial" Height="200px" Width="1000px" BorderColor="#999999" BorderStyle="Solid" ReadOnly="True"></asp:TextBox>
        <p style="font-family: Arial">
            Введите шаг сдвига и выберите направление сдвига:</p>
            <asp:TextBox ID="TextBox2" runat="server" Font-Names="Arial" BorderColor="#999999" BorderStyle="Solid"></asp:TextBox>
            <asp:RadioButtonList ID="RadioButtonList1" runat="server" Font-Names="Arial" BackColor="White" BorderColor="#CCCCCC" BorderStyle="Double">
                <asp:ListItem>Назад</asp:ListItem>
                <asp:ListItem Selected="True">Вперед</asp:ListItem>
            </asp:RadioButtonList>
            <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" style="height: 26px" Text="Шифровать" />
        <br />
            <asp:TextBox ID="TextBox3" runat="server" TextMode="MultiLine" Height="200px" ReadOnly="True" Width="1000px" Font-Names="Arial" BorderColor="#999999" BorderStyle="Solid"></asp:TextBox>
        <br />
            <asp:Button ID="Button3" runat="server" Font-Names="Arial" OnClick="Button3_Click" Text="Сохранить шифрованный текст" />
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:ImageButton ID="ImageButton1" runat="server" PostBackUrl="~/Home.aspx" BorderStyle="Ridge" Height="50px" ImageUrl="~/home.png" Width="50px" />
    </form>
</body>
</html>
