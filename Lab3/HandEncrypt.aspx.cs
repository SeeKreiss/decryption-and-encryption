﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Lab3
{
    public partial class HandEncrypt : System.Web.UI.Page
    {
        static string text;
        static int shift = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button4_Click(object sender, EventArgs e) //сохранение в текстовом формате
        {
            if (TextBox3.Text != "")
            {
                Response.ContentType = "text/plain";
                Response.AppendHeader("Content-Disposition", "attachment; filename=Decrypted.txt");
                Response.Write(TextBox3.Text);
                Response.End();
            }
        }

        protected void Button1_Click(object sender, EventArgs e) //шифрование
        {
            if (Int32.TryParse((TextBox2.Text), out shift))
            {
                if (shift >= 0)
                {
                    if (RadioButtonList1.SelectedIndex == 0)
                    {
                        shift *= -1;
                    }
                    if (shift < 0)
                    {
                        shift += 33;
                    }
                    text = TextBox1.Text;
                    TextBox3.Text = Crypto.Decrypto(text, shift);
                }
                else
                {
                    TextBox3.Text = "Введите положительное число";
                }
                
            }
            else
                TextBox3.Text = "Введите положительное число";

        }
    }


}
