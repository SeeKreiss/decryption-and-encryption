﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Decrypt.aspx.cs" Inherits="Lab3.Decrypt" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #TextArea1 {
            height: 34px;
        }
        .auto-style1 {
            width: 675px;
        }
        .auto-style2 {
            width: 541px;
        }
        .auto-style3 {
            width: 893px;
        }
        .auto-style4 {
            width: 1340px;
        }
        .auto-style5 {
            width: 158px;
            height: 27px;
        }
        .auto-style6 {
            width: 1225px;
            height: 27px;
        }
        .auto-style7 {
            width: 86px;
            margin-left: 920px;
        }
        .auto-style8 {
            width: 158px;
        }
        .auto-style9 {
            width: 1011px;
        }
    </style>
</head>
<body style="width: 1009px; margin-left: 300px;">
    <form id="form1" runat="server" class="auto-style9" style="border-style: solid; border-color: #999999">
    <div style="font-family: Arial">
             <table>
            <tr><td class="auto-style3" style="font-family: Arial" colspan="2">Чтобы загрузить файл для расшифровки нажмите кнопку &quot;Choose file&quot;.
                <br />
                Далее нажмите кнопку &quot;Показать&quot; чтобы отобразить текст.<br />
                По кнопкам &quot;&lt;&quot; и &quot;&gt;&quot; попробуйте найти связный текст, когда найдете, число между стрелками и будет ключем шифра.<br />
                <br />
                </td><td class="auto-style4" style="font-family: Arial">&nbsp;</td></tr>
             <tr><td class="auto-style5">Загрузить файл :</td><td class="auto-style6"><asp:FileUpload ID="FileUpload1" runat="server" accept=".txt, .docx" Font-Names="Arial" BorderColor="#999999" BorderStyle="Solid" /></td></tr>
             <tr><td colspan="1" class="auto-style8">&nbsp;</td>
                 <td><asp:Button ID="Button1" runat="server" Text="Показать" Height="24px" OnClick="Button1_Click" Width="127px" Font-Names="Arial" /></td>
                 </tr>
             <tr><td colspan="2"><asp:TextBox ID="cryptotxt" runat="server" TextMode="MultiLine" Width="1000px" Height="200px" Visible="false" ReadOnly="True" Font-Names="Arial" BorderColor="#999999" BorderStyle="Solid"></asp:TextBox></td></tr>
                 <tr>
                     <td class="auto-style1" colspan="2">
                         &nbsp;</td>
                 </tr>
                 
             <tr><td colspan="2" style="font-family: Arial">Используйте стрелки чтобы выбрать шаг сдвига.</td></tr>
             <tr><td colspan="2"><asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Width="1000px" Height="200px" Visible="false" ReadOnly="True" Font-Names="Arial" BorderColor="#999999" BorderStyle="Solid"></asp:TextBox></td></tr>
                 
                 <tr>
                     <td class="auto-style8">
                         <asp:Button ID="Button4" runat="server" Font-Names="Arial" OnClick="Button4_Click" Text="Сохранить дешифрованный текст" />
                     </td>
                 </tr>
                 
        </table>
        <p class="auto-style2" style="margin-left: 460px">
                         <asp:Button ID="Button2" runat="server" OnClick="Button2_Click1" Text="&lt;" />
                         <asp:TextBox ID="TextBox2" runat="server" Height="16px" style="margin-right: 0px" Width="48px" ReadOnly="True" BorderColor="#999999" BorderStyle="Solid"></asp:TextBox>
                         <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="&gt;" />
&nbsp;</p>
             <p class="auto-style7">
                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:ImageButton ID="ImageButton1" runat="server" PostBackUrl="~/Home.aspx" BorderStyle="Ridge" Height="50px" ImageUrl="~/home.png"  Width="50px" />
             </p>
    </div>
    </form>
</body>
</html>
